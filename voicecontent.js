/* jshint esversion: 6 */
/* global yaku,chrome,MutationObserver*/

'use strict';

let customSounds;
let mutationObserver = new MutationObserver(onMutate);;

const observerSettings = {
    characterData: true,
    childList: true,
    subtree: true,
};

function setToObserve() {
    mutationObserver.observe(document.documentElement, observerSettings);
}

function setOptions(options) {
    customSounds = options.customsounds;
    setToObserve();
}

chrome.runtime.onMessage.addListener(setOptions);

function checkNode(oneNode, doneRedeal) {
    if (!doneRedeal && oneNode.innerText && 
            (oneNode.innerText.substr(0,6) === 'Redeal' ||
            (oneNode.innerText[0] === '流' && oneNode.innerText[1] === '局'))) {
        // Won't work if all 4 hands are the same result
        let elem = document.getElementById('sc0');
        if (!elem) {
            return doneRedeal;
        }else if (elem.innerText.substr(-5,1) === '+') {
            chrome.runtime.sendMessage({ yaku: ['006'] }); // tenpai
        } else if (elem.innerText.substr(-5,1) === '-') {
            chrome.runtime.sendMessage({ yaku: ['007'] }); // noten
        }
        return true;
    }

    if (oneNode.childNodes.length && oneNode.childNodes[0].id === 'total') {
        let yakus = [];
        let textNodes = document.createTreeWalker(oneNode, NodeFilter.SHOW_TEXT, null, false);
        let doraCount = 0;
        let onDora = false;
        // now cycle over the yaku
        while (textNodes.nextNode()) {
            if (onDora) {
                onDora = false;
                doraCount += parseInt(textNodes.currentNode.nodeValue);
                continue;
            }
            let thisCode = yaku[textNodes.currentNode.nodeValue.trim()];
            if (thisCode) {
                if (thisCode === '228') {
                    onDora = true;
                } else {
                    yakus.push(thisCode);
                }
            }
        }
        for (let i = doraCount; i > 0; i -= 1) {
            yakus.push('228');
        }
        chrome.runtime.sendMessage({ yaku: yakus });
    }
    return doneRedeal;
}

function onMutate(mutations) {
    if (customSounds === 'DEFAULT') return;
    mutationObserver.disconnect();
    let doneRedeal = false;
    mutations.forEach(function processMutation(oneMutation) {
        if (oneMutation.addedNodes.length) {
            oneMutation.addedNodes.forEach(oneNode => doneRedeal = checkNode(oneNode, doneRedeal));
        }
    });
    setToObserve();
}

// This is what happens when the page is first loaded
chrome.storage.local.get({ customsounds: 'DEFAULT' }, (options) => {
    setToObserve();
    setOptions(options);
});

