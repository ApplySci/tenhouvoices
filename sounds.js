const yaku = {

    '兩立直': '201',
    '両立直': '201',
    'Double riichi': '201',

    '立直': '202',
    'Ready-riichi': '202',
    'Riichi': '202',

    '一發': '203',
    '一発': '203',
    'Ippatsu': '203',
    'One-shot': '203',

    '海底摸月': '204',
    'Haitei raoyue': '204',
    'Bottom of the sea (drawn)': '204',

    '河底撈魚': '205',
    'Houtei raoyui': '205',
    'Bottom of the sea (discard)': '205',

    '門前清自摸和': '206',
    'Menzen tsumo': '206',
    'Self-drawn fully concealed': '206',

    '嶺上開花': '207',
    'Rinshan kaihou': '207',
    'After a kong': '207',

    '槍槓': '208',
    'Chankan': '208',
    'Robbing a kong': '208',

    '斷么九': '209',
    '断幺九': '209',
    'Tanyao': '209',
    'All simples': '209',

    '平和': '210',
    'Pinfu': '210',
    'Pinfu no-point': '210',

    '一盃口': '211',
    'Iipeikou': '211',
    'Pure Double Chow': '211',

    '三色同順': '212',
    'Sanshoku doujun': '212',
    'Mixed Triple Chow': '212',

    '三色同刻': '213',
    'Sanshoku doukou': '213',
    'Triple Pung': '213',

    '一氣通貫': '214',
    '一気通貫': '214',
    'Ikkitsuukan': '214',
    'Pure straight': '214',

    '三暗刻': '215',
    'Sanankou': '215',
    '3 concealed pungs': '215',

    '三槓子': '216',
    'Sankantsu': '216',
    '3 kongs': '216',

    '對對和': '217',
    '対々和': '217',
    'Toitoi': '217',
    'All pungs': '217',

    '七對子': '218',
    '七対子': '218',
    'Chiitoitsu': '218',
    '7 pairs': '218',

    '小三元': '219',
    'Shousangen': '219',
    'Little 3 dragons': '219',

    '混一色': '220',
    'Honitsu': '220',
    'Half flush': '220',

    '混老頭': '221',
    'Honroutou': '221',
    'Only terminals & honours': '221',

    '混全帶么九': '222',
    '混全帯幺九': '222',
    'Chanta': '222',
    'Outside hand': '222',

    '純全帶么九': '223',
    '純全帯幺九': '223',
    'Junchan': '223',
    'Terminals in all sets': '223',

    '二盃口': '224',
    'Ryanpeikou': '224',
    'Twice pure double chow': '224',

    '清一色': '225',
    'Chiniisou': '225',
    'Chinitsu': '225',
    'Full flush': '225',

    '役牌 中': '227', // Yakuhai Chun
    'Yakuhai Chun': '227',
    'Red dragons': '227',
    '役牌 發': '227', // Yakuhai Hatsu
    'Yakuhai Hatsu': '227',
    'Green dragons': '227',
    '役牌 白': '227', // Yakuhai Haku
    'Yakuhai Haku': '227',
    'White dragons': '227',
    '自風 東': '227', // Jikaze Ton
    'Jikaze Ton': '227',
    'E Seat wind': '227',
    '自風 南': '227', // Jikaze Nan
    'Jikaze Nan': '227',
    'S Seat wind': '227',
    '自風 西': '227', // Jikaze Sha
    'Jikaze Sha': '227',
    'W Seat wind': '227',
    '自風 北': '227', // Jikaze Pei
    'Jikaze Pei': '227',
    'N Seat wind': '227',
    '場風 東': '227', // Bakaze Ton
    'Bakaze Ton': '227',
    'E Round Wind': '227',
    '場風 南': '227', // Bakaze Nan
    'Bakaze Nan': '227',
    'S Round wind': '227',
    '場風 西': '227', // Bakaze Sha
    'Bakaze Sha': '227',
    'W Round wind': '227',
    '場風 北': '227', // Bakaze Pei
    'Bakaze Pei': '227',
    'N Round wind': '227',

    'ドラ': '228',
    'Dora': '228',
    '赤ドラ': '228',
    'Akadora': '228',
    'Red fives': '228',
    '裏ドラ': '228',
    'Uradora': '228',

    '天和': '232',
    'Tenhou': '232',
    'Heaven\'s blessing': '232',

    '地和': '233',
    'Chiihou': '233',
    'Earth\'s blessing': '233',

    '國士無雙': '235',
    '国士無双': '235',
    'Kokushi musou': '235',
    '13 orphans': '235',
    '國士無雙13面': '235',
    '国士無双１３面': '235',
    'Kokushi musou 13 men': '235',
    '13 orphans (13 wait)': '235',

    '大三元': '236',
    'Daisangen': '236',
    '3 dragons': '236',

    '小四喜': '237',
    'Shousuushi': '237',
    'Little 4 winds': '237',

    '大四喜': '238',
    'Daisuushi': '238',
    'Big 4 winds': '238',

    '清老頭': '239',
    'Chinroutou': '239',
    'All terminals': '239',

    '四暗刻': '240',
    'Suuankou': '240',
    '4 concealed pungs': '240',
    '四暗刻單騎': '240',
    '四暗刻単騎': '240',
    'Suuankou tanki': '240',
    '4 concealed pungs (single wait)': '240',

    '四槓子': '241',
    'Suukantsu': '241',
    '4 kongs': '241',

    '字一色': '242',
    'Tsuuiisou': '242',
    'All honours': '242',

    '綠一色': '243',
    '緑一色': '243',
    'Ryuuiisou': '243',
    'All green': '243',

    '九蓮宝燈': '244',
    '九蓮寶燈': '244',
    'Chuuren poutou': '244',
    '9 gates': '244',
    '純正九蓮宝燈': '244',
    '純正九蓮寶燈': '244',
    'Junsei chuuren poutou': '244',
    '9 gates (9 wait)': '244',

};

/* .
Now uses these too, on redeals (except where all players have the same outcome):
    '': '006', tenpai
    '': '007', noten

Unused for now:
    '連風牌': '226',  // Renpuuhai - combined seat & round wind. Hard to do in tenhou, which lists them separately

No way to use these in tenhou that I can see - not sure where reaction voices would fit into the game
   '100', ohmoji
   '101', kau
   '102', kambarihoa
   '103', itahokare
   '104', riichi
   '105', tsumo
*/