/* jshint esversion: 6 */
/* globals chrome */
document.addEventListener('DOMContentLoaded', ignored => {
    
    'use strict';
    const NUMBER_OF_VOICES = 100;

    let thisForm = document.forms.optionform;

    function updateWithNewOptions(options) {
        chrome.runtime.sendMessage(options);
        chrome.tabs.query({ url: '*://tenhou.net/*' }, function(tabs) {
            for (let tab of tabs) {
                chrome.tabs.sendMessage(tab.id, options);
            }
        });
    }

    function saveOptions() {
        const options = { customsounds: thisForm.customsounds.value };
        chrome.storage.local.set(options, () => updateWithNewOptions(options));
    }

    // keep track of which slots are valid, and use the first valid one as the default

    let newDefault = NUMBER_OF_VOICES;
    let remainingCalls = new Array(NUMBER_OF_VOICES);
    let missingRequirements = new Array(NUMBER_OF_VOICES);
    remainingCalls.fill(2);
    missingRequirements.fill(2);
    let resolvedCount = 2 * NUMBER_OF_VOICES;
    
    function callbackFunc(label, i, ok) {
        return () => {
            remainingCalls[i] -= 1;
            resolvedCount -= 1;
            if (ok) {
                missingRequirements[i] -= 1;
            }
            if (!remainingCalls[i]) {
                if (missingRequirements[i]) {
                    label.style.display = 'none';
                    console.log('bad ' + i);
                } else {
                    newDefault = i < newDefault ? i : newDefault;
                    label.style.display = 'inline-block';
                    console.log('got ' + i);
                }
            }
            if (!resolvedCount) {
                
                // resolved all the potential icons

                chrome.storage.local.get({
                    customsounds: false
                }, function(items) {
                    if (typeof(items) ==='undefined' || items.customsounds === false || missingRequirements[items.customsounds]) {
                        thisForm.customsounds.value = newDefault === NUMBER_OF_VOICES ? 'DEFAULT' : newDefault;
                        saveOptions();
                    } else {
                        thisForm.customsounds.value = items.customsounds;
                        updateWithNewOptions(items);
                    }
                });
            }
        };
    }

    // find which of the slots have valid files, and remove from display any that are invalid
    for (let i = 0; i < NUMBER_OF_VOICES;i += 1) {

        const id = ('0' + i).substr(-2,2); // NB this assumes NUMBER_OF_VOICES <= 100

        let label = document.createElement('label');

        let radioButton = document.createElement('input');
        radioButton.type = 'radio';
        radioButton.name = 'customsounds';
        radioButton.value = id;

        let holder = document.createElement('div');

        let sample = new Image();
        sample.addEventListener('error', callbackFunc(label, i, false));
        sample.addEventListener('load', callbackFunc(label, i, true));
        sample.src = 'pic/'+ id + '.png';

        let audio = new Audio('wav/mv_' + id + '_000.wav');
        audio.addEventListener('error', callbackFunc(label, i, false));
        audio.addEventListener('canplay', callbackFunc(label, i, true));

        holder.appendChild(sample);
        label.appendChild(radioButton);
        label.appendChild(holder);
        thisForm.appendChild(label);
    }

    let inputs = document.getElementsByTagName('input');
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener('change', saveOptions);
    }

});
