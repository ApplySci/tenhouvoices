/* jshint esversion: 6 */
/* globals chrome */

chrome.runtime.onInstalled.addListener(function(object) {
    // show options screen on install
    if (chrome.runtime.OnInstalledReason.INSTALL === object.reason) {
        chrome.runtime.openOptionsPage();
    }
});

// show the addon button for one tab
function showIconForTab(tab) {
    if (tab.url.includes('tenhou.net')) {
        chrome.runtime.onMessage.addListener(getOptions);
        chrome.pageAction.show(tab.id);
    }
}

// on initialisation, show the addon button for all tenhou tabs
chrome.tabs.query({ url: '*://tenhou.net/*' }, (tabs) => {
    for (let tab of tabs) {
        showIconForTab(tab);
    }
});

chrome.tabs.onUpdated.addListener((id, changeInfo, tab) => showIconForTab(tab));

let customSounds = '00';

function getWavPath(id) {
    return 'wav/mv_' + customSounds + '_' + id + '.wav';
}

function getOptions(options) {
    if (options.customsounds) {
        customSounds = options.customsounds;
        return;
    }
    if (customSounds === 'DEFAULT') return false;

    let i = 0;
    function playOne() {
        let audio = new Audio(getWavPath(options.yaku[i]));
        i += 1;
        if (i < options.yaku.length) {
            audio.addEventListener('ended', () => setTimeout(playOne, 150) ); // pause between files
        }
        audio.play();
    }
    if (options.yaku) {
        setTimeout(playOne, 600);
    }

}

// listen for messages about the user changing their options or making sounds
chrome.runtime.onMessage.addListener(getOptions);

function getWavURI(id) {
    return { redirectUrl: chrome.extension.getURL(getWavPath(id)) };
}

// Replace sounds with custom sounds
chrome.webRequest.onBeforeRequest.addListener((details) => {
    const spriteUrlRegex = /([^\/]*).wav/;
    const matches = spriteUrlRegex.exec(details.url);
    switch (matches[1]) {
        case '01':
        case '10':
        case '11':
            return { redirectUrl: chrome.extension.getURL('wav/silent.wav') };
        case '13': // pon
            return getWavURI('000');
        case '15': // kan
            return getWavURI('002');
        case '17': // chi
            return getWavURI('001');
        case '19': // riichi
            return getWavURI('003');
        case '21': // ron
            return getWavURI('005');
        case '23': // tsumo
            return getWavURI('004');
        default:
            return;            
    }
}, {
    urls: ['*://tenhou.net/3/snd/01.wav','*://tenhou.net/3/snd/1*.wav', '*://tenhou.net/3/snd/2*.wav'],
    types: ['media', 'object', 'other','xmlhttprequest'],
}, ['blocking']);

// on load of extension
chrome.storage.local.get({ customsounds: '00' }, getOptions);
